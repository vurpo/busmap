"use strict";

Object.filter = (obj, predicate) => 
    Object.keys(obj)
          .filter( key => predicate(obj[key]) )
          .reduce( (res, key) => (res[key] = obj[key], res), {} );

var mmlAttribution = '&copy; <a href="https://www.maanmittauslaitos.fi">Maanmittauslaitos</a>, <a href="https://creativecommons.org/licenses/by/4.0/">CC-BY 4.0</a>';
var digitransitAttribution = '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA 2.0</a>'

var taustakartta = L.tileLayer('https://avoin-karttakuva.maanmittauslaitos.fi/avoin/wmts/1.0.0/{mapId}/default/WGS84_Pseudo-Mercator/{z}/{y}/{x}.png', {
	attribution: mmlAttribution,
	maxZoom: 18,
    mapId: 'taustakartta'
});

var maastokartta = L.tileLayer('https://avoin-karttakuva.maanmittauslaitos.fi/avoin/wmts/1.0.0/{mapId}/default/WGS84_Pseudo-Mercator/{z}/{y}/{x}.png', {
	attribution: mmlAttribution,
    maxZoom: 18,
    mapId: 'maastokartta'
});

var reittiopas = L.tileLayer('https://cdn.digitransit.fi/map/v1/{mapId}/{z}/{x}/{y}{r}.png', {
    attribution: digitransitAttribution,
    maxZoom: 19,
    tileSize: 512,
    zoomOffset: -1,
    mapId: 'hsl-map'
});

var reittiopas_sv = L.tileLayer('https://cdn.digitransit.fi/map/v1/{mapId}/{z}/{x}/{y}{r}.png', {
    attribution: digitransitAttribution,
    maxZoom: 19,
    tileSize: 512,
    zoomOffset: -1,
    mapId: 'hsl-map-sv'
});

var mymap = L.map('mapview', {
    center: [60.45, 22.27],
    zoom: 13,
    zoomSnap: 0,
});

reittiopas.addTo(mymap);

L.control.layers({
    "Reittiopas": reittiopas,
    "Reittiopas på svenska": reittiopas_sv,
    "MML Taustakartta": taustakartta,
    "MML Maastokartta": maastokartta,
}, null).addTo(mymap);

L.control.locate({
    "showPopup": false
}).addTo(mymap);

var siriUrl = "https://data.foli.fi/siri/vm";

var busmarkers = {};
var buspopups = {};

function newBus(bus) {
    var icon = L.divIcon({className: 'busIcon', html: bus.publishedlinename, iconAnchor: [12.4, 12.4], popupAnchor: [0, -15]});
    return L.marker([bus.latitude, bus.longitude], {icon: icon});
}

var liikennoitsijat = {
    "2": "TLO",
    "6": "Jalo Bus",
    "8": "TuKL",
    "11": "Savonlinja",
    "16": "Turkubus",
    "18": "Muurinen",
    "20": "Nyholm",
    "27": "Citybus",
    "28": "Päivölä",
    "41": "Länsilinjat",
    "55": "V-S Bussipalvelut",
    "67": "Vainio",
    "68": "Vesma",
    "69": "Airisto Line",
    "70": "Explore Saimaa"
};

function formatBusID(busID) {
    if (busID.slice(0,-4) in liikennoitsijat) {
        return liikennoitsijat[busID.slice(0,-4)] + ' ' + parseInt(busID.slice(-4));
    } else {
        return busID;
    }
}

function generatePopupContents(busID, bus) {
    return '<b>'+bus.publishedlinename+'</b> '+bus.destinationname+'<br />'+
        formatBusID(busID);
}

function fetchBuses() {
    var req = new XMLHttpRequest();
    req.onreadystatechange = function() { 
        if (req.readyState == 4 && req.status == 200) {
            var response = JSON.parse(req.responseText);
            if (response.status == "OK") {
                document.getElementById("error").style.visibility = 'hidden';
                var buses = Object.filter(response.result.vehicles, v => v.monitored);
                for (var busID in busmarkers) {
                    if (!(busID in buses)) {
                        busmarkers[busID].remove();
                        delete busmarkers[busID];
                        delete buspopups[busID];
                    }
                }
                for (var busID in buses) {
                    if (busID in busmarkers) {
                        busmarkers[busID].setLatLng([buses[busID].latitude, buses[busID].longitude]);
                        buspopups[busID].setContent(generatePopupContents(busID, buses[busID]));
                    } else {
                        buspopups[busID] = L.popup().setContent(generatePopupContents(busID, buses[busID]));
                        busmarkers[busID] = newBus(buses[busID]).bindPopup(buspopups[busID]).addTo(mymap);
                    }
                }
            } else {
                document.getElementById("error").style.visibility = 'visible';
            }
        }
    }
    req.open("GET", siriUrl, true); // true for asynchronous 
    req.send(null);
}

fetchBuses();
setInterval(fetchBuses, 3000);
